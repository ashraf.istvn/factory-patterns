package abstractFactory;

public abstract class AbstructFactory {
	public abstract Car getCar(String type);
	public abstract Suv getSuv(String type);
	
	public void display() {
		System.out.print("I am Abstract factory class");
	}
}
