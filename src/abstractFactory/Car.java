package abstractFactory;

public interface Car {
	String getCar();
}