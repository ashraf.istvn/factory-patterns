package abstractFactory;

public class FactoryGenerator {
	
	public static AbstructFactory getFactory(String type) {
		
		if (type.equalsIgnoreCase("bmw")) {
			return new BMWFactory();
		}
		
		if (type.equalsIgnoreCase("toyota")) {
			return new ToyotaFactory();
		}
		
		return null;
	}
}
