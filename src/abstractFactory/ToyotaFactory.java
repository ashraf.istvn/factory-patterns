package abstractFactory;

public class ToyotaFactory extends AbstructFactory{

	@Override
	public Car getCar(String type) {
		
		if (type.contentEquals("car")) {
			return new ToyotaCar();
		}
		return null;
	}

	@Override
	public Suv getSuv(String type) {
		// TODO Auto-generated method stub
		
		if (type.contentEquals("suv")) {
			return new ToyotaSuv();
		}
		
		return null;
	}

}
