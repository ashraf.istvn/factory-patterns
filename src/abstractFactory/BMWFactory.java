package abstractFactory;

public class BMWFactory extends AbstructFactory{

	@Override
	public Car getCar(String type) {
		
		if (type.contentEquals("car")) {
			return new BMWCar();
		}
		return null;
	}

	@Override
	public Suv getSuv(String type) {
		// TODO Auto-generated method stub
		
		if (type.contentEquals("suv")) {
			return new BMWSuv();
		}
		return null;
	}

}
