package abstractFactory;

/**
 * Abstract factory method 
 * 
 * @author ashraf789
 * ashraf.istvn@gmail.com
 * date: 27.07.2020
 */
public class Main {
	public static void main(String arg[]) {
		
		// runtime object creation, all are loosely coupled
		AbstructFactory bmwFactory = FactoryGenerator.getFactory("bmw");
		AbstructFactory toyotaFactory = FactoryGenerator.getFactory("toyota");
		
		Car toyotaCar = toyotaFactory.getCar("car");
		Suv toyotaSuv = toyotaFactory.getSuv("suv");
		
		Car bmwCar = bmwFactory.getCar("car");
		Suv bmwSuv = bmwFactory.getSuv("suv");
		
		// display the output
		System.out.println(toyotaCar.getCar());
		System.out.println(toyotaSuv.getSuv());
		System.out.println(bmwCar.getCar());
		System.out.println(bmwSuv.getSuv());

	}
}