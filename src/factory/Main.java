package factory;

/**
 * Factory method
 * 
 * @author ashraf789
 * ashraf.istvn@gmail.com
 * date: 27.07.2020
 */
public class Main {
	public static void main(String arg[]) {
		
		// compile time object creation
		Factory factory = new Factory();
		
		// runtime car object creation (loosely coupled)
		Car sportCar = factory.getCar("sport");
		Car superCar = factory.getCar("super");
		Car luxuryCar = factory.getCar("luxury");
		
		// display the output
		System.out.println(superCar.getCar());
		System.out.println(sportCar.getCar());
		System.out.println(luxuryCar.getCar());
	}
}
