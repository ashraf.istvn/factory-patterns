package factory;

public class Factory {
	public Car getCar(String type) {
		
		if(type.equalsIgnoreCase("luxury")) {
			return new LuxuryCar();
		}
		
		if(type.equalsIgnoreCase("sport")) {
			return new SportCar();
		}
		
		if(type.equalsIgnoreCase("super")) {
			return new SuperCar();
		}
		
		if(type.equalsIgnoreCase("mini")) {
			return new MiniCar();
		}
		
		return null;
	}
}
